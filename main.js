window.addEventListener('DOMContentLoaded', function () {

	// Déclare des variables pour le nombre d'essais, le nombre à deviner, et le meilleur score
	let tries;
	let randomNumber;
	const bestScore = localStorage.getItem("bestScore");

	// Récupère les éléments du DOM par leur ID
	const bestScoreElement = document.getElementById("best-score-value");
	const form = document.getElementById("guess_form");

	if (bestScore != null) {
		// Met à jour le contenu de l'élément avec le meilleur score
		bestScoreElement.textContent = bestScore;
	}

	// Fonction pour initialiser le jeu
	function startGame() {

		tries = 1; // Réinitialise le nombre d'essais à 1
		randomNumber = Math.floor(Math.random() * 100); // Génère un nombre aléatoire entre 1 et 100

		console.log('Le nombre à deviner est :' + randomNumber); // Je rajoute ici un console.log() qui permet d'afficher le nombre qui a été tiré au hasard
	}

	startGame(); // Appelle la fonction pour démarrer le jeu au chargement de la page

	// Fonction pour gérer la soumission du formulaire
	function submitForm(event) {
		event.preventDefault(); // Empêche le rechargement de la page lors de la soumission
		console.log('soumission du formulaire');

		const value = form.guess.value; // Récupère la valeur entrée par l'utilisateur et la convertit en nombre entier
		console.log(value);

		if (value > randomNumber) {
			tries++ // Incrémente le nombre d'essais
			console.log('tries :' + tries);

			text.textContent = "Too high, try again !" // Indique que le nombre est trop grand
		}
		else if (value < randomNumber) {
			tries++ // Incrémente le nombre d'essais
			console.log('tries :' + tries);

			text.textContent = "Too low, try again !" // Indique que le nombre est trop bas
		}

		else {
			text.textContent = "Well done !" // Indique que le nombre est bon

			localStorage.setItem("bestScore", tries); // Enregistre le nouveau meilleur score dans le localStorage
		}
	}
	form.addEventListener('submit', submitForm); // Écoute l'événement de soumission du formulaire
})




